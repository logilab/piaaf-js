'use strict';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const _ = require('lodash');

const output = path.resolve(path.join(__dirname, 'dist'));
const source = path.resolve(path.join(__dirname, 'src'));

const config = (module.exports = {
    entry: [path.join(source, 'index.js')],
    output: {
        filename: 'piaaf.js',
        path: output,
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: require.resolve('sigma'),
                use: 'imports-loader?this=>window',
            },
            {
                test: require.resolve('sigma/plugins/sigma.layout.forceAtlas2/worker.js'),
                use: 'imports-loader?this=>window',
            },
            {
                test: require.resolve('sigma/plugins/sigma.layout.forceAtlas2/supervisor.js'),
                use: 'imports-loader?this=>window',
            },
            {
                test: require.resolve(
                    'sigma/plugins/sigma.renderers.customShapes/shape-library.js'
                ),
                use: 'imports-loader?this=>window',
            },
            {
                test: require.resolve(
                    'sigma/plugins/sigma.renderers.customShapes/sigma.renderers.customShapes.js'
                ),
                use: 'imports-loader?this=>window',
            },
        ],
    },
    resolve: {
        extensions: ['.js'],
        alias: {
            sigmaForceLayoutWorker: 'sigma/plugins/sigma.layout.forceAtlas2/worker.js',
            sigmaForceLayoutSupervisor: 'sigma/plugins/sigma.layout.forceAtlas2/supervisor.js',
            sigmaShapeLibrary: 'sigma/plugins/sigma.renderers.customShapes/shape-library.js',
            sigmaCustomShapes:
                'sigma/plugins/sigma.renderers.customShapes/sigma.renderers.customShapes.js',
            jquery: path.join(source, 'jquery-stub.js'),
        },
    },
    externals: {
        YASGUI: 'YASGUI',
    },
    plugins: [
        // new webpack.optimize.CommonsChunkPlugin("bundle-ext.js"),
        new webpack.IgnorePlugin(/^(buffertools)$/), // unwanted "deeper" dependency
    ],
});

if (process.env.NODE_ENV === 'production') {
    config.entry = _.chain(config.entry)
        .map((v, k) => [k, ['babel-polyfill', v]])
        .fromPairs()
        .value();

    config.plugins.push(
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production'),
            },
        })
    );
} else {
    const DEFAULT_SPARQL_ENDPOINT_URL = 'http://localhost:8890/sparql/';
    const SPARQL_ENDPOINT_URL = process.env.SPARQL_ENDPOINT_URL || DEFAULT_SPARQL_ENDPOINT_URL;

    config.plugins.push(
        new webpack.ProvidePlugin({
            sigma: 'sigma',
        }),
        new HtmlWebpackPlugin({
            title: 'PIAAF',
            template: path.join(source, 'index.ejs'),
        }),
        new CopyWebpackPlugin([{ from: path.join(source, 'static'), to: 'static' }]),
        new webpack.DefinePlugin({
            SPARQL_ENDPOINT_URL: JSON.stringify(SPARQL_ENDPOINT_URL),
        })
    );

    config.devServer = {
        contentBase: output,
        compress: true,
        host: '0.0.0.0',
        port: 4201,
        disableHostCheck: true,
        historyApiFallback: { disableDotRule: true },
    };
}
