import React from 'react';
import { NavLink as Link } from 'react-router-dom';

export const FIRST_CLASS_RESOURCES = {
    // XXX we don't handle plural forms in translations, therefore we need to
    // hardcode them here. Besides, some labels are supposed to be different in
    // the navigation bar from their canonical translation.
    GroupType: 'Catégories de collectivités',
    LegalStatus: 'Statuts juridiques',
    FunctionAbstract: "Domaines d'activité",
    Position: 'Postes',
    CorporateBody: 'Collectivités',
    Person: 'Personnes',
    TopRecordSet: 'Groupes de documents',
};

export function NotFound() {
    return (
        <div>
            <h3>404 not found</h3>
            <p>cette page n&apos;existe pas.</p>
        </div>
    );
}

function NavLink(props) {
    return <Link {...props} activeClassName="active" />;
}

function Header() {
    return (
        <nav className="navbar navbar-default">
            <div className="container-fluid">
                <div className="navbar-header">
                    <button
                        type="button"
                        className="navbar-toggle"
                        data-toggle="collapse"
                        data-target="#main-navbar">
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                    </button>
                    <NavLink to={'/'} className="navbar-brand navbar-brand-long" exact>
                        Pilote d’Interopérabilité pour les Autorités Archivistiques Françaises –
                        PIAAF
                    </NavLink>
                    <NavLink to={'/'} className="navbar-brand navbar-brand-small" exact>
                        PIAAF
                    </NavLink>
                </div>
                <div id="main-navbar" className="collapse navbar-collapse">
                    <ul className="nav navbar-nav">
                        <li>
                            <NavLink to={'/sparql'}>SPARQL</NavLink>
                        </li>
                        <li>
                            <NavLink to={'/alignments'}>Alignements</NavLink>
                        </li>
                        <li>
                            <NavLink to={'/editorial/help'}>Utiliser le prototype</NavLink>
                        </li>
                    </ul>
                    <form className="navbar-form navbar-right" action="/search">
                        <div className="form-group">
                            <input
                                name="q"
                                type="text"
                                className="form-control"
                                placeholder="Rechercher"/>
                        </div>
                        <button type="submit" className="btn btn-default">
                            Ok
                        </button>
                    </form>
                    <ContentHeader />
                </div>
            </div>
        </nav>
    );
}

function ContentHeader() {
    return (
        <ul className="nav nav-pills clear">
            {Array.prototype.map.call(Object.keys(FIRST_CLASS_RESOURCES), ricType => (
                <li key={ricType}>
                    <NavLink to={`/ric/${ricType}`}>{FIRST_CLASS_RESOURCES[ricType]}</NavLink>
                </li>
            ))}
        </ul>
    );
}

function Footer() {
    return (
        <footer className="footer">
            <NavLink to={'/editorial/credits'} exact>
                Crédits
            </NavLink>
            <NavLink to={'/editorial/legal'} exact>
                Mentions légales
            </NavLink>
            <div id="partners">
                <a id="logomcc" href="http://www.culturecommunication.gouv.fr/" target="_blank">
                    <img src="/static/images/logo-mcc.png" />
                </a>
                <a id="logoan" href="http://www.archivesnationales.culture.gouv.fr/"
                    target="_blank">
                    <img src="/static/images/logo-an.png" />
                </a>
                <a id="logobnf" href="http://www.bnf.fr" target="_blank">
                    <img src="/static/images/logo-bnf.png" />
                </a>
                <a id="logosiaf" href="https://francearchives.fr" target="_blank">
                    <img src="/static/images/logo-siaf.png" />
                </a>
            </div>
        </footer>
    );
}

export function App(props) {
    return (
        <div>
            <Header />
            <div className="container">{props.children}</div>
            <Footer />
        </div>
    );
}
