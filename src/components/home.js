import React from 'react';
import { last } from 'lodash';
import { NavLink } from 'react-router-dom';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

import {
    reprefix,
    sparql,
    sparqlGraphQuery,
    sparqlSKOSGraphQuery,
    localResourceURI,
    GRAPH_NAMES,
    propLabel,
} from '../sparql';
import { Loading } from './helpers';
import { SigmaRDFGraph } from './graph';

// search in query string as provided by react-router in `props.location.search`
// for param named `searchedParam` and return its value or null if not found
export function extractParamValue(searchString, searchedParam) {
    if (!searchString) {
        return null;
    }
    for (const paramDef of searchString.substring(1).split('&')) {
        const [param, value] = paramDef.split('=');
        if (param === searchedParam) {
            return decodeURIComponent(value);
        }
    }
    return null;
}

function resourceFormatter(cell, row) {
    if (row.provider) {
        return (
            <NavLink to={`${localResourceURI(row.url)}`}>
                {cell} <span className="provider">@{row.provider}</span>
            </NavLink>
        );
    } else {
        return <NavLink to={`${localResourceURI(row.url)}`}>{cell}</NavLink>;
    }
}

function resourceTypeFormatter(cell, row) {
    if (!cell) {
        return <span></span>;
    }
    if (row.etypeuri) {
        return <NavLink to={row.etypeuri}>{row.etypelabel}</NavLink>;
    } else {
        return <span>{cell}</span>;
    }
}

export function RDFResourceCollectionView(props) {
    const etypes = {};
    const lines = Array.prototype.map.call(props.bindings, binding => {
            const url = binding.resource.value,
                label = binding.label ? binding.label.value : reprefix(url),
                provider = binding.graph ? GRAPH_NAMES[binding.graph.value] : '',
                etype = binding.etype ? reprefix(binding.etype.value) : null;
            let etypeuri, etypelabel;
            if (etype) {
                const chunks = etype.split(':');
                if (chunks.length === 2 && chunks[0] === 'ric') {
                    etypeuri = `/ric/${last(etype.split(':'))}`;
                }
                etypes[etype] = propLabel(etype);
                etypelabel = propLabel(etype);
            }
            return { label, url, provider, etype: etype, etypelabel, etypeuri };
        }),
        title = props.title;
    const withEtypeCol = !!Object.keys(etypes).length;
    return (
        <BootstrapTable data={lines} pagination>
            {withEtypeCol &&
                <TableHeaderColumn
                    dataField="etype"
                    dataFormat={resourceTypeFormatter}
                    filter={{ type: 'SelectFilter', options: etypes }}
                    width="20%">
                    Type
            </TableHeaderColumn>
            }
            <TableHeaderColumn
                dataField="label"
                dataFormat={resourceFormatter}
                filter={{
                    type: 'TextFilter',
                    delay: 1000,
                    placeholder: 'tapez ici pour restreindre sur le libellé',
                }}
                width={withEtypeCol ? '80%' : null}
                isKey={true}>
                {title}
            </TableHeaderColumn>
        </BootstrapTable>
    );
}

const RIC_SKOS_SCHEME_URL = {
    LegalStatus: 'http://piaaf.demo.logilab.fr/resource/legal-statuses/ls',
};

function resourcesListQuery(ricType) {
    // ricType may not be a real resource type but rather reference to a concept
    // scheme
    if (RIC_SKOS_SCHEME_URL[ricType] !== undefined) {
        return `
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT ?resource ?label WHERE {
    ?resource a skos:Concept;
              skos:prefLabel ?label;
              skos:inScheme <${RIC_SKOS_SCHEME_URL[ricType]}>
}
ORDER BY ?label, ?resource`;
    } else {
        return `
PREFIX ric: <http://www.ica.org/standards/RiC/ontology#>

SELECT ?graph ?resource ?label WHERE {
    GRAPH ?graph {
        ?resource a ric:${ricType}.
        OPTIONAL {?resource rdfs:label ?label}
    }
}
ORDER BY ?label, ?resource`;
    }
}

class SPARQLResourcesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { bindings: null, title: 'Ressources' };
        this.value = this.valueFromProps(props);
    }

    componentWillReceiveProps(nextProps) {
        const newValue = this.valueFromProps(nextProps);
        if (newValue !== this.value) {
            this.setState({ bindings: null });
            this.value = newValue;
            this.fetchData();
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    render() {
        if (this.state.bindings === null) {
            return <Loading />;
        }
        return (
            <RDFResourceCollectionView bindings={this.state.bindings} title={this.state.title} />
        );
    }
}

export class RICResourcesList extends SPARQLResourcesList {
    valueFromProps(props) {
        return props.ricType;
    }

    fetchData() {
        sparql(resourcesListQuery(this.value)).then(bindings => {
            this.setState({ bindings });
        });
    }
}

export class SearchResults extends SPARQLResourcesList {
    valueFromProps(props) {
        return extractParamValue(props.location.search, 'q');
    }

    fetchData() {
        let term = this.value;
        if (term.split(' ').length === 1) {
            term = `"${term}*"`;
        }
        sparql(`
SELECT ?resource ?label ?graph ?etype WHERE {
    GRAPH ?graph {
        ?resource a ?etype.
        ?resource ?p ?o.
        ?o bif:contains '${term}'
        OPTION (score ?sc).
        OPTIONAL {?resource rdfs:label ?resourceRDFSLabel.}
        OPTIONAL {?resource skos:prefLabel ?resourceSKOSLabel.}
        BIND (COALESCE(?resourceSKOSLabel, ?resourceRDFSLabel) as ?label)
    }
}
ORDER BY DESC (?sc)`).then(bindings => {
    let title;
    if (!bindings.length) {
        title = `Aucun résultat pour ${term}`;
    } else if (bindings.length === 1) {
        title = `1 résultat pour ${term}`;
    } else {
        title = `${bindings.length} résultats pour ${term}`;
    }
    this.setState({ bindings, title });
});
    }
}

export function RICResourcesGraph(props) {
    let rdfType = `ric:${props.ricType}`;
    let query;
    if (RIC_SKOS_SCHEME_URL[props.ricType] === undefined) {
        query = sparqlGraphQuery(`${rdfType} as ?entityType`);
    } else {
        query = sparqlSKOSGraphQuery(RIC_SKOS_SCHEME_URL[props.ricType]);
        rdfType = 'skos:Concept';
    }
    return (
        <SigmaRDFGraph
            query={query}
            fromBaseSet={node => node.rdfType === rdfType}
            inputType={rdfType} />
    );
}

export function RICResources(props) {
    const ricType = props.match.params.type;
    // if (FIRST_CLASS_RESOURCES[ricType] === undefined) {
    //     return <NotFound />;
    // }
    return (
        <div>
            <RICResourcesGraph ricType={ricType} />
            <RICResourcesList ricType={ricType} />
        </div>
    );
}
