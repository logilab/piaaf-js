/* global fetch */

import React from 'react';
import { NavLink } from 'react-router-dom';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

import { reprefix, sparql, listQueries, localResourceURI } from '../sparql';

function resourceFormatter(cell) {
    return <NavLink to={`${localResourceURI(cell)}`}>{reprefix(cell)}</NavLink>;
}

function buildAlignButtonFormatter(confirmCallback) {
    return function ConfirmAlignment(cell, row) {
        // eslint-disable-line react/display-name
        const confirmClosure = _ => confirmCallback(row.resource1, row.resource2, 'sameAs'),
            infirmClosure = _ => confirmCallback(row.resource1, row.resource2, 'differentFrom');
        return (
            <div>
                <button
                    className="btn btn-default"
                    title="cliquez pour indiquer que ce sont bien deux ressources identiques"
                    onClick={confirmClosure}>
                    <span className="glyphicon glyphicon-ok" />
                </button>
                <button
                    className="btn btn-default"
                    title="cliquez pour indiquer que ce sont deux ressources différentes"
                    onClick={infirmClosure}>
                    <span className="glyphicon glyphicon-remove" />
                </button>
            </div>
        );
    };
}

export class Alignments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alignments: [],
            alignmentData: null,
        };
        this.alignmentChanged = this.alignmentChanged.bind(this);
        this.confirmAlignment = this.confirmAlignment.bind(this);
    }

    componentDidMount() {
        listQueries('Alignment').then(alignments => {
            this.setState({ alignments });
        });
    }

    render() {
        const alignments = this.state.alignments;
        let alignmentHTML;
        if (this.state.alignmentData === null) {
            alignmentHTML = (
                <div className="alignHelp text-muted">
                    Veuillez sélectionnez un alignement ci-dessus
                </div>
            );
        } else if (this.state.alignmentData === undefined) {
            alignmentHTML = <div className="alignHelp text-muted">Alignement en cours...</div>;
        } else {
            alignmentHTML = (
                <BootstrapTable data={this.state.alignmentData} pagination condensed>
                    <TableHeaderColumn dataField="resource1" dataFormat={resourceFormatter}>
                        Ressource 1
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="resource2" dataFormat={resourceFormatter}>
                        Ressource 2
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="explanation">Explication</TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="key"
                        dataFormat={buildAlignButtonFormatter(this.confirmAlignment)}
                        dataAlign="right"
                        isKey={true}>
                        Action
                    </TableHeaderColumn>
                </BootstrapTable>
            );
        }
        return (
            <div>
                {Array.prototype.map.call(alignments, alignment => (
                    <span
                        className="alignment btn btn-default"
                        key={alignment.title}
                        title={"cliquez pour lancer l'alignement"}
                        data-sparql={alignment.sparql}
                        onClick={this.alignmentChanged}>
                        {alignment.title}
                    </span>
                ))}
                {alignmentHTML}
            </div>
        );
    }

    alignmentChanged(event) {
        this.setState({ alignmentData: undefined });
        sparql(event.target.getAttribute('data-sparql')).then(bindings => {
            const alignmentData = Array.prototype.map.call(bindings, binding => {
                return {
                    resource1: binding.resource1.value,
                    resource2: binding.resource2.value,
                    key: [binding.resource1.value, binding.resource2.value],
                    explanation: binding.explanation.value,
                };
            });
            this.setState({ alignmentData: alignmentData });
        });
    }

    confirmAlignment(resource1, resource2, relation) {
        const query = `
INSERT INTO GRAPH <urn:piaaf:data> {
    <${resource1}> owl:${relation} <${resource2}>
}`;
        sparql(query).then(_ => {
            const alignmentData = Array.prototype.filter.call(this.state.alignmentData, align => {
                return !(align.resource1 === resource1 && align.resource2 === resource2);
            });
            this.setState({ alignmentData });
        });
    }
}
